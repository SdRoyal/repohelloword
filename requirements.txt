appdirs==1.4.4
asgiref==3.2.7
certifi==2020.4.5.1
chardet==3.0.4
distlib==0.3.0
dj-database-url==0.5.0
Django==3.0.6
django-admin-rangefilter==0.6.0
django-heroku==0.3.1
django-settings-export==1.2.1
django-tinymce==3.0.2
filelock==3.0.12
gunicorn==20.0.4
pipenv==2018.11.26
psycopg2==2.8.5
pytz==2020.1
requests==2.23.0
six==1.15.0
sqlparse==0.3.1
urllib3==1.25.9
virtualenv==20.0.21
virtualenv-clone==0.5.4
whitenoise==5.1.0
